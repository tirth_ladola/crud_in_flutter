import 'dart:convert';

import 'package:api_call/insertdata.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiCall extends StatefulWidget {
  const ApiCall({Key? key}) : super(key: key);

  @override
  State<ApiCall> createState() => _ApiCallState();
}

class _ApiCallState extends State<ApiCall> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Api Call Demo"),
          actions: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                  return InsertData(map: null,);
                },),).then((value) {
                  setState(() {

                  });
                },);
              },
              child: Icon(Icons.add),
            ),
          ],
        ),
        body: FutureBuilder<http.Response>(
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              dynamic jsonData = jsonDecode(snapshot.data!.body.toString());
              return ListView.builder(
                itemCount: jsonData.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                        return InsertData(map: jsonData[index]);
                      },),).then((value) {
                        setState(() {

                        });
                      },);
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: ListTile(
                        tileColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        title: Text(jsonData[index]['PrinterName'].toString()),
                        subtitle:
                            Text(jsonData[index]['PrinterMaker'].toString()),
                        leading: CircleAvatar(
                          backgroundImage:
                              NetworkImage(jsonData[index]['PrinterImage']),
                        ),
                        trailing: InkWell(
                          onTap: () {
                            // deletePrinter(jsonData[index]['id']).then(
                            //   (value) {
                            //     setState(() {});
                            //   },
                            // );
                            showAlertDialog(context , jsonData[index]['id']);
                          },
                          child: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
          future: getPrinterData(),
        ));
  }

  Future<http.Response> getPrinterData() async {
    http.Response res = await http
        .get(Uri.parse('https://632140c382f8687273ae92a3.mockapi.io/Printer'));
    print(res.body.toString());

    return res;
  }

  Future<void> deletePrinter(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://632140c382f8687273ae92a3.mockapi.io/Printer/$id'));
  }

  showAlertDialog(BuildContext context , jsonData) {

    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed:  () {
        deletePrinter(jsonData).then((value) {
          Navigator.of(context).pop();
          setState(() {

          });
        },);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are You Sure Want Delete ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


}
