import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertData extends StatefulWidget {
  InsertData({Key? key , this.map}) : super(key: key);

  Map? map;

  @override
  State<InsertData> createState() => _InsertDataState();
}

class _InsertDataState extends State<InsertData> {

  var formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController modelController = TextEditingController();
  TextEditingController imageController = TextEditingController();

  bool isloading = false;

  @override
  void initState() {

    nameController.text = widget.map == null?'':widget.map!["PrinterName"];
    modelController.text = widget.map == null?'':widget.map!["PrinterMaker"];
    imageController.text = widget.map == null?'':widget.map!["PrinterImage"];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Printer"),
      ),
      body: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter Name";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: modelController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter Model";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: imageController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter Image";
                  }
                },
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    isloading = true;
                  });
                  if(formKey.currentState!.validate()){
                    if(widget.map == null){
                      inserUser().then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                    else{
                      updateUser(widget.map!['id']).then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                  }
                },
                child: Container(
                  width: 180,
                  height: 50,
                  padding: EdgeInsets.only(top: 10 , right: 50 , bottom: 10 , left: 50),
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: isloading?CircularProgressIndicator():Text('Submit', style: TextStyle(color: Colors.white),),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  
  Future<void> inserUser() async {

    Map map= {};
    map["PrinterName"] = nameController.text.toString();
    map['PrinterMaker'] = modelController.text.toString();
    map['PrinterImage'] = imageController.text.toString();

    http.Response res = await http.post(Uri.parse('https://632140c382f8687273ae92a3.mockapi.io/Printer'),body: map);
    print(res.body);
  }

  Future<void> updateUser(id) async {

    Map map= {};
    map["PrinterName"] = nameController.text.toString();
    map['PrinterMaker'] = modelController.text.toString();
    map['PrinterImage'] = imageController.text.toString();

    http.Response res = await http.put(Uri.parse('https://632140c382f8687273ae92a3.mockapi.io/Printer/$id'),body: map);
    print(res.body);
  }

}
